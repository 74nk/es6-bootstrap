import {expect} from 'chai'
import {hello} from '../src/hello'

describe('Simple Test', () => {
    it('should welcome', () => {
        expect(hello()).to.be.equal('Hello World!');
    });
});

